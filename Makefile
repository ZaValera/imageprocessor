BUILD_PATH = $(shell cat build_path)

start:
	$(BUILD_PATH)/devBuild/electron.exe --isDev

%:
	$(MAKE) -C web "$@"