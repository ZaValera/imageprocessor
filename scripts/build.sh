#!/bin/bash

declare -x buildPath

RED='\033[0;31m'
Green='\033[0;32m'
NC='\033[0m'

# 1. Переменные
mainDir=$(dirname "$(dirname "$(readlink -f "$0")")")
. "$mainDir"/scripts/export.sh || exit 1
name=$(awk 'match($0, /"name": "(.*)"/, a) {print a[1]}' "$mainDir"/web/package.json)

# 2. Замена иконки
echo -e "${Green}Updating icon...${NC}"

if ! [ -f "$mainDir"/icon.ico ]
then
  echo -e "${RED}ERROR: Can't find '$mainDir/icon.ico'${NC}"
  exit 1
fi

cp "$mainDir"/scripts/rcedit-x64.exe "$mainDir"/tmp/rcedit-x64.exe
cp "$mainDir"/icon.ico "$mainDir"/tmp/icon.ico
cd "$mainDir"/tmp || exit 1
./rcedit-x64.exe electron.exe --set-icon icon.ico
rm -f rcedit-x64.exe
rm -f icon.ico
cd - || exit 1
echo -e "${Green}Icon successfully updated${NC}"

# 3. Переименование .exe файла
mv "$mainDir"/tmp/electron.exe "$mainDir"/tmp/"$name".exe

# 4. Архивация app дирректории
cd "$mainDir"/tmp/resources || exit 1
"$mainDir"/web/node_modules/.bin/asar pack app app.asar --unpack-dir "{$(paste -sd "," "$mainDir"/asar_unpack),}"
cd - || exit 1

rm -rf "${mainDir:?}"/tmp/resources/app
rm -rf "${buildPath:?}"/"$name"
mv "${mainDir:?}"/tmp "$buildPath"/"$name"
echo -e "${Green}Finished successfully ${NC}"