#!/bin/bash

declare -x buildPath

Green='\033[0;32m'
NC='\033[0m'

echo -e "${Green}Coping electron folder...${NC}"

mainDir=$(dirname "$(dirname "$(readlink -f "$0")")")
. "$mainDir"/scripts/export.sh || exit 1
electronFolder=$(find "$mainDir"/* -maxdepth 1 -type d -name "electron-*" -printf "%f\n")

rm -rf "${mainDir:?}"/tmp || exit 1
cp -r "$mainDir/$electronFolder" "$mainDir"/tmp
mkdir "$mainDir"/tmp/resources/app
cp "$mainDir"/web/package.json "$mainDir"/tmp/resources/app/package.json
cp "$mainDir"/web/yarn.lock "$mainDir"/tmp/resources/app/yarn.lock
cd "$mainDir"/tmp/resources/app || exit 0
npm_config_platform=win32 "$mainDir"/web/yarn/bin/yarn install --offline --production --pure-lockfile
cd - || exit 0
rm -f "$mainDir"/tmp/resources/app/yarn.lock

echo -e "${Green}Electron folder successfully copied${NC}"
