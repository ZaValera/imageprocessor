const path = require('path');
const fs = require('fs');

exports.getCommonLoaders = function (dirname, isDev) {
    const loaders = [];

    return loaders;
};

exports.getBuildPath = function (dirname, isDev) {
    if (isDev) {
        try {
            const buildPath = fs.readFileSync(path.resolve(__dirname, '../../build_path'), {encoding: 'utf8'}).trim();

            return path.resolve(buildPath, 'devBuild/resources/app/build/');
        } catch (e) {
            return path.resolve(dirname, '../build/');
        }
    } else {
        return path.resolve(dirname, '../../tmp/resources/app/build/');
    }
}

exports.getParams = function (env) {
    const isDev = env && env.dev;
    const dirname = env && env.dirname || __dirname;
    const mode = isDev ? 'development' : 'production';
    const devtool = isDev ? 'eval-cheap-module-source-map' : 'source-map';
    const storybook = env && env.storybook;

    return {
        isDev,
        dirname,
        mode,
        devtool,
        storybook,
    };
};