const path = require('path');
const nodeExternals = require('webpack-node-externals');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const {getCommonLoaders, getParams, getBuildPath} = require('./webpack.utils');

module.exports = env => {
    const {
        isDev,
        dirname,
        mode,
        devtool,
    } = getParams(env);

    return {
        mode,
        devtool,
        target: 'electron-main',
        cache: true,
        entry: {
            index: './src/back/index.ts',
            preload: './src/back/preload.ts',
        },
        output: {
            filename: '[name].js',
            path: path.resolve(getBuildPath(dirname, isDev), 'back'),
            publicPath: '/build/',
        },
        node: {
            __dirname: false,
        },
        externals: [nodeExternals()],
        module: {
            rules: [
                {
                    test: /\.ts$/,
                    include: [path.resolve(dirname, 'back'), path.resolve(dirname, 'shared')],
                    use: [
                        ...getCommonLoaders(dirname, isDev),
                        {
                            loader: 'ts-loader',
                            options: {
                                transpileOnly: true,
                                configFile: path.resolve(dirname, 'back/tsconfig.json'),
                            },
                        },
                    ],
                },
            ],
        },
        resolve: {
            extensions: ['.ts', '.js'],
            alias: {
                src: path.resolve(dirname, './back'),
                shared: path.resolve(dirname, './shared'),
            },
        },
        plugins: [
            new ForkTsCheckerWebpackPlugin({
                typescript: {
                    configFile: path.resolve(dirname, 'back/tsconfig.json'),
                }
            }),
            new FriendlyErrorsWebpackPlugin(),
        ],
    };
};