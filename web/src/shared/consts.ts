import moment from 'moment';

export const DATE_FORMAT = 'DD.MM.YYYY';

export const DATE_TIME_FORMAT = 'DD.MM.YYYY HH:mm';

export enum EFolders {
    newFolder = 'newFolder',
    backupFolder = 'backupFolder',
}

export interface IElectronAPI {
    openFile: () => Promise<string>;
    openFolder: (name: EFolders) => Promise<string>;
    compare: () => Promise<string[]>;
    copy: (data: any[]) => Promise<string|Error>;
}

export interface IImageItem {
    id: number;
    origName: string;
    newName: string;
    path: string;
    exist: {
        path: string;
        name: string;
        date: string;
        base64: string;
    };
    blob: Buffer;
    base64: string;
}
