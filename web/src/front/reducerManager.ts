import {
    combineReducers,
    Reducer,
    StateFromReducersMapObject,
    ActionFromReducersMapObject,
} from '@reduxjs/toolkit';

const initialReducers: IReducers = {
    init: () => ({}), // заглушка, так как стора пустой быть не должна, ругаетяс ReduxDevtools
    // здесь перечисляются внешние редьюсеры
};

export function createReducerManager() {
    const reducers = {...initialReducers};
    let combinedReducer = combineReducers(reducers);
    let keysToRemove: string[] = [];

    return {
        getCombinedReducer: () => combinedReducer,

        // ToDo: replaceReducer - убрать, если получится перейти
        reduce: (state: TState, action: TAction) => {
            if (keysToRemove.length > 0) {
                state = {...state};

                for (let key of keysToRemove) {
                    delete state[key];
                }

                keysToRemove = [];
            }

            return combinedReducer(state, action);
        },

        add: (key: string, reducer: Reducer) => {
            if (!key || reducers[key]) {
                return;
            }

            reducers[key] = reducer;
            combinedReducer = combineReducers(reducers);

            return combinedReducer;
        },

        remove: (key: string) => {
            if (!key || !reducers[key]) {
                return;
            }

            delete reducers[key];
            keysToRemove.push(key);
            combinedReducer = combineReducers(reducers);

            return combinedReducer;
        },
    }
}

interface IReducers {
    [key: string]: Reducer;
}

type TState = StateFromReducersMapObject<IReducers>;
type TAction = ActionFromReducersMapObject<IReducers>;
