import 'src/styles/import.css';
import 'src/styles/index.scss';
import React from 'react';
import ReactDOM from 'react-dom';
import {MemoryRouter as Router} from 'react-router-dom';
import {Provider} from 'react-redux'
import {ThemeProvider, createTheme} from '@mui/material/styles';
import AdapterDateFns from '@mui/lab/AdapterMoment';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import {QueryClient, QueryClientProvider} from 'react-query';
import moment from 'moment';
import {configureStore} from '@reduxjs/toolkit';

import {Layout} from 'src/layout/Layout';
import {createReducerManager} from 'src/reducerManager';

export const reducerManager = createReducerManager()
const combinedReducer = reducerManager.getCombinedReducer();

export const store = configureStore({
    reducer: reducerManager.reduce,
    /*middleware: (getDefaultMiddleware) => {
        return getDefaultMiddleware({
            serializableCheck: {
                ignoreActions: true,
            },
        });
    },*/
    // reducer: combinedReducer, //ToDo: replaceReducer - альтернативный ваариант, на который ругается React Devtools
});

const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            refetchOnWindowFocus: false,
            staleTime: Infinity,
            retry: false,
        },
    },
});

window.store = store;
window.moment = moment;
moment.locale('ru');

const theme = createTheme({
    typography: {
        fontFamily: 'PT Sans Regular, sans-serif',
        fontWeightMedium: 600,
    },
    components: {
        MuiButton: {
            styleOverrides: {
                root: {
                    fontWeight: 500,
                },
            }
        },
    },
});

ReactDOM.render(
    <Provider store={store}>
        <QueryClientProvider client={queryClient}>
            <ThemeProvider theme={theme}>
                <Router>
                    <LocalizationProvider dateAdapter={AdapterDateFns}>
                        <Layout/>
                    </LocalizationProvider>
                </Router>
            </ThemeProvider>
        </QueryClientProvider>
    </Provider>,
    document.getElementById('root')
);

export type TRootState = ReturnType<typeof combinedReducer>
