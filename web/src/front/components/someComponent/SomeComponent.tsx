import React from 'react';
import styles from './someComponent.scss';
import {DATE_FORMAT} from 'shared/consts';
import moment from 'moment';

export function SomeComponent(props: IProps) {
    return (
        <div className={styles.someComponent}>
            Some Component {props.text} {moment().format(DATE_FORMAT)}
        </div>
    );
}

interface IProps {
    text: string;
}
