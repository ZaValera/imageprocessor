import React, {useEffect, useMemo, useContext} from 'react';
import {bindActionCreators} from 'redux';
import {useSelector, useDispatch} from 'react-redux';
import {
    createSlice,
    Slice,
    CreateSliceOptions,
    ValidateSliceCaseReducers,
    SliceCaseReducers,
    CaseReducerActions
} from '@reduxjs/toolkit';
import {reducerManager, store, TRootState} from 'src/index';
import _ from 'lodash';

const slicesMap: ISlicesMap = {};

export function createUseSliceHook<Options extends CreateSliceOptions>(getSliceData: () => Options) {
    type State = Options['initialState'];
    type CaseReducers = Options['reducers'];
    type Name = Options['name'];
    type TSlice = Slice<State, CaseReducers, Name>;

    const sliceData = getSliceData();

    return function (sliceName?: string): {
        slice: TSlice,
        state: State,
        actions: TSlice['actions'],
    } {
        const name = useMemo(() => sliceName || _.uniqueId(`${sliceData.name}_`), [sliceName]);

        let sliceItem = slicesMap[name];

        if (!sliceItem) {
            const slice = createSlice<State, CaseReducers, Name>({
                initialState: sliceData.initialState,
                reducers: sliceData.reducers as ValidateSliceCaseReducers<State, CaseReducers>,
                name,
            })

            sliceItem = slicesMap[name] = {
                slice,
                usedCount: 0,
            };

            reducerManager.add(name, sliceItem.slice.reducer);
            store.dispatch({type: 'addSlice', name}); // чтобы обновился state
            // store.replaceReducer(reducerManager.getCombinedReducer()); // ToDo: replaceReducer - альтернативный ваариант, на который ругается React Devtools
        }

        useEffect(() => {
            sliceItem.usedCount++;

            return () => {
                const sliceItem = slicesMap[name];

                if (--sliceItem.usedCount === 0) {
                    delete slicesMap[name];
                    reducerManager.remove(name);
                    store.dispatch({type: 'removeSLice', name}); // чтобы обновился state
                    // store.replaceReducer(reducerManager.getCombinedReducer()); // ToDo: replaceReducer - альтернативный ваариант, на который ругается React Devtools
                }
            }
        }, []);

        return useSlice(sliceItem.slice);
    };
}

function useSliceState<S>(slice: Slice<S>): S {
    return useSelector((state: TRootState) => state[slice.name]);
}

function useSliceActions<S, CR extends SliceCaseReducers<S>>(slice: Slice<S, CR>): CaseReducerActions<CR> {
    const dispatch = useDispatch();

    return useMemo(() => (
        bindActionCreators<any, any>(slice.actions, dispatch)
    ), [
        slice.actions,
        dispatch,
    ]);
}

export function useSlice<S, CR extends SliceCaseReducers<S>>(slice: Slice<S, CR> | React.Context<Slice<S, CR>>): {slice: Slice, state: S, actions: CaseReducerActions<CR>} {
    let sl;

    if ('Provider' in slice) {
        sl = useContext(slice);
    } else {
        sl = slice;
    }

    return {
        slice: sl,
        state: useSliceState(sl),
        actions: useSliceActions(sl),
    };
}

interface ISlicesMap {
    [key: string]: {
        slice: Slice;
        usedCount: number;
    }
}
