import React, {useState, useMemo, useEffect} from 'react';
import styles from './mainPage.scss';
import _ from 'lodash';
import {MainContext, useMainSlice} from './mainSlice';
import {Controls} from 'src/pages/main/components/controls/Controls';
import {NewItemsTable} from 'src/pages/main/components/newItemsTable/NewItemsTable';
import {IImageItem} from 'shared/consts';
import {toggleArrayItem} from 'shared/utils';
import {ExistItemsTable} from 'src/pages/main/components/existItemsTable/ExistItemsTable';


export function MainPage() {
    const {slice, actions} = useMainSlice();

    const [result, setResult] = useState(null);
    const [checkedItems, setCheckedItems] = useState<IImageItem[]>([]);

    const {existItems, newItems} = useMemo(() => {
        const [existItems, newItems] = _.partition(result, item => item.exist);

        return {
            newItems,
            existItems,
        };
    }, [result]);

    useEffect(() => {
        setCheckedItems(newItems);
    }, [result, setCheckedItems]);

    useEffect(() => {
        actions.setCount(checkedItems.length);
    }, [checkedItems, actions.setCount]);

    return (
        <MainContext.Provider value={slice}>
            <div className={styles.mainPage}>
                <Controls
                    canCopy={result && checkedItems.length > 0}
                    onCompare={async (cb) => {
                        const data = await window.electronAPI.compare();
                        cb();
                        setResult(data);
                    }}
                    onCopy={async () => {
                        const data = await window.electronAPI.copy(checkedItems.map(item => item.id));

                        actions.setCopyResult(data);
                    }}
                />
                {result &&
                    <div>
                        <div className={styles.header}>New Items</div>
                        <div className={styles.imagesWrap}>
                            <NewItemsTable
                                items={newItems}
                                checkedItems={checkedItems}
                                onChange={(checked: boolean, item: IImageItem) => {
                                    setCheckedItems(toggleArrayItem(checkedItems, item, checked));
                                }}
                            />
                        </div>
                        <div className={styles.header}>Exist Items</div>
                        <div className={styles.imagesWrap}>
                            <ExistItemsTable
                                items={existItems}
                                checkedItems={checkedItems}
                                onChange={(checked: boolean, item: IImageItem) => {
                                    setCheckedItems(toggleArrayItem(checkedItems, item, checked));
                                }}
                            />
                        </div>
                    </div>
                }
            </div>
        </MainContext.Provider>
    );
}
