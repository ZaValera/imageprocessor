import React from 'react';
import {PayloadAction} from '@reduxjs/toolkit';
import {createUseSliceHook} from 'src/sliceHook';

const initialState: ISliceState = {
    copyResult: null,
    count: 0,
};

const reducers = {
    setCopyResult(state: ISliceState, action: PayloadAction<ISliceState['copyResult']>) {
        state.copyResult = action.payload;
    },
    setCount(state: ISliceState, action: PayloadAction<ISliceState['count']>) {
        state.count = action.payload;
    },
};

export function getSliceData() {
    return {
        name: 'main',
        initialState,
        reducers,
    };
}

export const useMainSlice = createUseSliceHook(getSliceData);

export const MainContext = React.createContext<TSlice>(null);

export interface ISliceState {
    copyResult: string|Error;
    count: number;
}

type TSlice = ReturnType<typeof useMainSlice>['slice'];
