import React from 'react';
import styles from './existItemsTable.scss';
import {
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Paper,
    Checkbox,
} from '@mui/material';
import {IImageItem} from 'shared/consts';


export function ExistItemsTable(props: IProps) {
    const {items, checkedItems, onChange} = props;

    return (
        <TableContainer component={Paper}>
            <Table sx={{minWidth: 880}}>
                <TableHead>
                    <TableRow>
                        <TableCell>Select</TableCell>
                        <TableCell>New</TableCell>
                        <TableCell>Backup</TableCell>
                        <TableCell align="right">New Name</TableCell>
                        <TableCell align="right">Original Name</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {items.map((item) => (
                        <TableRow key={item.newName}>
                            <TableCell>
                                <Checkbox
                                    checked={checkedItems.includes(item)}
                                    onChange={(event) => {
                                        onChange(event.target.checked, item);
                                    }}
                                    inputProps={{ 'aria-label': 'controlled' }}
                                />
                            </TableCell>
                            <TableCell>
                                <img
                                    alt={item.origName}
                                    className={styles.img}
                                    src={`file:///${item.path}`}
                                />
                            </TableCell>
                            <TableCell>
                                <img
                                    alt={item.newName}
                                    className={styles.img}
                                    src={`file:///${item.exist.path}`}
                                />
                            </TableCell>
                            <TableCell align="right">{item.newName}</TableCell>
                            <TableCell align="right">{item.origName}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}

interface IProps {
    items: IImageItem[];
    checkedItems: IImageItem[];
    onChange: (checked: boolean, item: IImageItem) => void;
}