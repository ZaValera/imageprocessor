import React, {useState} from 'react';
import styles from './controls.scss';
import {EFolders} from 'shared/consts';
import {
    Button,
    Stack,
} from '@mui/material';
import {LoadingButton} from '@mui/lab';
import {MainContext} from 'src/pages/main/mainSlice';
import {useSlice} from 'src/sliceHook';

export function Controls(props: IProps) {
    const {canCopy} = props;
    const {state} = useSlice(MainContext);
    const {copyResult, count} = state;
    const [backupFolder, setBackupFolder] = useState<string>(null);
    const [newFolder, setNewFolder] = useState<string>(null);
    const [loadingCompare, setLoadingCompare] = useState<boolean>(false);

    return (
        <Stack spacing={2}>
            <Stack
                className={styles.buttonRow}
                spacing={2}
                direction={'row'}
            >
                <Button
                    size={'small'}
                    variant={'contained'}
                    onClick={async () => {
                        const data = await window.electronAPI.openFolder(EFolders.backupFolder);

                        if (data) {
                            setBackupFolder(data);
                        }
                    }}
                >
                    Backup folder
                </Button>
                {backupFolder && <div>{backupFolder}</div>}
            </Stack>
            <Stack
                className={styles.buttonRow}
                spacing={2}
                direction={'row'}
            >
                <Button
                    size={'small'}
                    variant={'contained'}
                    onClick={async () => {
                        const data = await window.electronAPI.openFolder(EFolders.newFolder);

                        if (data) {
                            setNewFolder(data);
                        }
                    }}
                >
                    New folder
                </Button>
                {newFolder && <div>{newFolder}</div>}
            </Stack>
            <Stack
                className={styles.buttonRow}
                spacing={2}
                direction={'row'}
            >
                <LoadingButton
                    loading={loadingCompare}
                    style={{
                        width: 100,
                    }}
                    color='success'
                    size='small'
                    variant='contained'
                    disabled={!backupFolder || !newFolder}
                    onClick={() => {
                        setLoadingCompare(true);
                        props.onCompare(() => {
                            setLoadingCompare(false);
                        });
                    }}
                >
                    Compare
                </LoadingButton>
                <Button
                    style={{
                        width: 140,
                    }}
                    color='success'
                    size='small'
                    variant='contained'
                    disabled={!canCopy}
                    onClick={props.onCopy}
                >
                    Copy Selected ({count})
                </Button>
                {copyResult === 'success' &&
                    <div className={styles.success}>SUCCESS</div>
                }
                {copyResult && copyResult !== 'success' &&
                    <div className={styles.error}>ERROR</div>
                }
            </Stack>
        </Stack>
    );
}

interface IProps {
    onCompare: (cb: () => void) => void;
    onCopy: () => void;
    canCopy: boolean;
}