import React, {lazy} from 'react';
import {Route, Navigate, Routes} from 'react-router-dom';


const MainPage = lazy(() => import(
    /* webpackChunkName: "pages/MainPage" */
    'src/pages/main'
    )
);

export function Router() {
    return (
        <Routes>
            <Route path='/' element={<Navigate to='/main'/>}/>
            <Route path='/main' element={<MainPage/>}/>
        </Routes>
    );
}