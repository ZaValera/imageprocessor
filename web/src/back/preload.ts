import {contextBridge, ipcRenderer} from 'electron';

contextBridge.exposeInMainWorld('electronAPI', {
    openFile: () => ipcRenderer.invoke('dialog:openFile'),
    openFolder: (name: string) => ipcRenderer.invoke('dialog:openFolder', name),
    compare: () => ipcRenderer.invoke('compare'),
    copy: (data: any[]) => ipcRenderer.invoke('copy', data),
})