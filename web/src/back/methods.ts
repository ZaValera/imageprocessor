import {dialog, IpcMainInvokeEvent} from 'electron';
import {readdir, copyFile} from 'fs/promises';
import path from 'path';
import sharp from 'sharp';
import {EFolders} from 'shared/consts';
import {getExifData} from 'src/backUtils';


let imageId = 1;

const folders: Record<EFolders, string> = {
    newFolder: null,
    backupFolder: null,
};

let imagesToCopy: Record<string, {name: string; path: string}> = {};

export async function handleFolderOpen(e: IpcMainInvokeEvent, name: EFolders) {
    const data = await dialog.showOpenDialog({
        properties: ['openDirectory'],
    });

    const {canceled, filePaths} = data;

    if (!canceled) {
        const path = filePaths[0];
        folders[name] = path;

        return path;
    }
}

export async function handleCopy(e: IpcMainInvokeEvent, data: number[]) {
    try {
        for (const id of data) {
            const item = imagesToCopy[id];

            await copyFile(item.path, path.join(folders.backupFolder, item.name));
        }

        return 'success';
    } catch (e) {
        return e;
    }
}

export async function handleCompare() {
    try {
        const backupItems = await readdir(folders.backupFolder);
        const newItems = await readdir(folders.newFolder);

        const resNewItems = [];
        const resBackupItems = [];

        imagesToCopy = {};

        for (const item of backupItems) {
            const itemPath = path.join(folders.backupFolder, item);
            const exifData = await getExifData(itemPath);
            const date = exifData.exif.CreateDate;

            /*const smallImage = await sharp(itemPath)
                .withMetadata()
                .resize({height: 200})
                .toBuffer();*/

            resBackupItems.push({
                name: item,
                date,
                path: itemPath.replaceAll('\\', '/'),
                orientation: exifData.image.Orientation,
                // base64: smallImage.toString('base64'),
            });
        }

        for (const item of newItems) {
            const itemPath = path.join(folders.newFolder, item);
            const exifData = await getExifData(itemPath);
            const extension = item.split('.')[1];
            const date = exifData.exif.CreateDate;
            const newName = `${date.replaceAll(':', '-')}.${extension}`;
            const alreadyExist = resBackupItems.find(item => item.date === date);
            const id = imageId++;

            /*const smallImage = await sharp(itemPath)
                .withMetadata()
                .resize({height: 200})
                .toBuffer();*/

            imagesToCopy[id] = {
                name: newName,
                path: itemPath,
            };

            resNewItems.push({
                id,
                origName: item,
                newName,
                path: itemPath.replaceAll('\\', '/'),
                exist: alreadyExist,
                orientation: exifData.image.Orientation,
                // base64: smallImage.toString('base64'),
            });
        }

        return resNewItems;
    } catch (e) {
        console.log(e);
    }
}