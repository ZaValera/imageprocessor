import {app, BrowserWindow, ipcMain} from 'electron';
import path from 'path';
import {handleFolderOpen, handleCompare, handleCopy} from 'src/methods';


function createWindow () {
    const mainWindow = new BrowserWindow({
        width: 1000,
        height: 700,
        webPreferences: {
            preload: path.join(__dirname, 'preload.js')
        }
    });

    if (process.argv.includes('--isDev')) {
        mainWindow.maximize();
        mainWindow.webContents.openDevTools();
    }

    mainWindow.loadFile(path.join(__dirname, '../front/index.html'));
}

app.whenReady().then(() => {
    ipcMain.handle('dialog:openFolder', handleFolderOpen);
    ipcMain.handle('compare', handleCompare);
    ipcMain.handle('copy', handleCopy);

    createWindow();
})

app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') app.quit();
})