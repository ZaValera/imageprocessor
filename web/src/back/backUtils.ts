import fs from 'fs';
import path from 'path';
import {ExifImage, ExifData} from 'exif';
import {getLogString} from 'shared/utils';


const logFile = fs.createWriteStream(path.resolve(__dirname, '../../../../main_process.log'), {flags : 'a+'});

export function log(pref: string, str: string, data?: any, timeStart?: number) {
    const resStr = getLogString(pref, str, timeStart);

    logFile.write(resStr + '\n');
    consoleLog(resStr);

    if (data) {
        const dataStr = JSON.stringify(data, (key, value) => (value instanceof Map ? [...value] : value));
        logFile.write(dataStr + '\n');
        consoleLog(dataStr);
    }

    return resStr;
}

export function consoleLog(data: string|Object) {
    let str;

    if (typeof data === 'string') {
        str = data;
    } else {
        str = JSON.stringify(data, null, 2);
    }

    console.log(str.replaceAll('\n', '\r\n'), '\r');
}

export function getExifData(file: Buffer | string) {
    return new Promise<ExifData>((resolve, reject) => {
        new ExifImage(file, (error, exifData) => {
            if (error) {
                reject();
            } else {
                resolve(exifData);
            }
        })
    });
}
