const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const {getCommonLoaders, getParams, getBuildPath} = require('./webpack.utils');


module.exports = env => {
    const {
        isDev,
        dirname,
        mode,
        devtool,
        storybook,
    } = getParams(env);

    let plugins = [
        new ForkTsCheckerWebpackPlugin({
            typescript: {
                configFile: path.resolve(dirname, 'front/tsconfig.json'),
            }
        }),
        new MiniCssExtractPlugin({
            filename: 'css/[name].[contenthash].css',
            chunkFilename: 'css/[id].[contenthash].css',
        }),
    ];

    if (!storybook) {
        plugins.push(
            new HtmlWebpackPlugin({
                inject: true,
                filename: 'index.html',
                favicon: path.resolve(dirname, './front/assets/images/base.png'),
                template: path.resolve(dirname, './front/index.html'),
                title: 'Base',
            }),
            new FriendlyErrorsWebpackPlugin(),
        );
    }

    const commonLoaders = getCommonLoaders(dirname, isDev);

    const entry = ['./src/front/index.tsx'];

    return {
        mode,
        devtool,
        plugins,
        entry,
        target: 'electron-renderer',
        cache: true,
        output: {
            filename: '[id].[contenthash].js',
            chunkFilename: '[id].[contenthash].js',
            path: path.resolve(getBuildPath(dirname, isDev), 'front'),
            globalObject: 'window',
        },
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    include: [path.resolve(dirname, 'front'), path.resolve(dirname, 'shared')],
                    use: [
                        ...commonLoaders,
                        {
                            loader: 'ts-loader',
                            options: {
                                transpileOnly: true,
                                configFile: path.resolve(dirname, 'front/tsconfig.json'),
                            },
                        },
                    ],
                },
                {
                    test: /\.(png|svg|jpg|gif)$/,
                    include: path.resolve(dirname, 'front/assents/images'),
                    type: 'asset/resource',
                    generator: {
                        filename: 'images/[name]_[hash].[ext]'
                    },
                },
                {
                    test: /\.(eot|ttf|woff)$/,
                    include: path.resolve(dirname, 'front/assents/fonts'),
                    type: 'asset/resource',
                    generator: {
                        filename: 'css/fonts/[name]_[hash].[ext]'
                    },
                },
                {
                    test: /\.css$/,
                    include: path.resolve(dirname, 'front'),
                    use: [
                        MiniCssExtractPlugin.loader,
                        ...commonLoaders,
                        'css-loader',
                    ],
                },
                {
                    test: /\.s[ac]ss$/i,
                    include: path.resolve(dirname, 'front'),
                    use: [
                        MiniCssExtractPlugin.loader,
                        ...commonLoaders,
                        {
                            loader: 'css-loader',
                            options: {
                                sourceMap: isDev,
                                importLoaders: 3,
                                modules: {
                                    localIdentName: isDev ? '[name]--[local]--[hash:base64:6]' : '[hash:base64:6]',
                                    exportLocalsConvention: 'camelCase',
                                },
                            },
                        },
                        {
                            loader: 'resolve-url-loader',
                        },
                        {
                            loader: 'sass-loader',
                            options: {
                                sourceMap: true,
                                implementation: require('sass'),
                                sassOptions: {
                                    includePaths: ['../node_modules'],
                                },
                            },
                        },
                        {
                            loader: 'sass-resources-loader',
                            options: {
                                resources: [
                                    path.resolve(dirname, 'front/styles/vars.scss'),
                                    path.resolve(dirname, 'front/styles/mixins.scss'),
                                ],
                            },
                        },
                    ],
                },
            ],
        },
        resolve: {
            extensions: ['.js', '.tsx', '.ts'],
            modules: [
                path.resolve(dirname, '../node_modules'),
            ],
            alias: {
                src: path.resolve(dirname, 'front'),
                shared: path.resolve(dirname, 'shared'),
                assets: path.resolve(dirname, 'front/assets'), // для import в ts
                './front': path.resolve(dirname, 'front'), // для url() в scss
            },
            symlinks: false,
            cacheWithContext: false,
        }
    };
};